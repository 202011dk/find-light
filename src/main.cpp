#include <Arduino.h>

#define SENSORS_COUNT 5
#define BUTTON_PIN 19
#define LEARN_READY_LED_PIN 22
#define LEARN_MODE_LED_PIN 21
int sensorPins[] = {35, 34, 32, 27, 26};

float sensorValues[SENSORS_COUNT];
int sensorDarkValues[SENSORS_COUNT];
int sensorLightValues[SENSORS_COUNT];

int buttonState = LOW;
int buttonPressCounter = 0;
long lastTimeReadSensors = 0;
int learnMode = 0;
bool buttonPressed = false;

void setup() {
    Serial.begin(115200);
    for (int index = 0; index < SENSORS_COUNT; index++) {
        pinMode(sensorPins[index], INPUT);
    }

    pinMode(BUTTON_PIN, INPUT);
    pinMode(LEARN_READY_LED_PIN, OUTPUT);
    pinMode(LEARN_MODE_LED_PIN, OUTPUT);
}

void setLearnLedStates() {
    switch (learnMode) {
        case 0: {
            digitalWrite(LEARN_READY_LED_PIN, LOW);
            digitalWrite(LEARN_MODE_LED_PIN, LOW);
            break;
        }
        case 1: {
            digitalWrite(LEARN_READY_LED_PIN, HIGH);
            digitalWrite(LEARN_MODE_LED_PIN, LOW);
            break;
        }
        case 2: {
            digitalWrite(LEARN_READY_LED_PIN, HIGH);
            digitalWrite(LEARN_MODE_LED_PIN, HIGH);
            break;
        }
    }
}

void readSensors() {
    for (int index = 0; index < SENSORS_COUNT; index++) {
        float val = analogRead(sensorPins[index]);
        sensorValues[index] = (val - sensorDarkValues[index]) / (sensorLightValues[index] - sensorDarkValues[index]);
    }
}
//----------------------------------------------------------------

void analyzeSensors() {
    if (lastTimeReadSensors < (millis() - 200)) {
        lastTimeReadSensors = millis();

        readSensors();
        //Проходим по массиву и сравниваем
        float maxValue = sensorValues[0];
        int indexOfMaxValue = 0;
        for (int index = 1; index < SENSORS_COUNT; index++) {
            // имеющееся у на максимальное значение с значением следующего элемента массива
            if (maxValue < sensorValues[index]) {
                maxValue = sensorValues[index];
                // найден элемент больший, чем ранее найде
                // запоминаем индекс нового максимального значения
                indexOfMaxValue = index;
            }
        }

// пробежим по массиву значений сеносоров и сбросим ВСЕ значения в ноль
        for (int index = 0; index < SENSORS_COUNT; index++) {
            sensorValues[index] = 0;
        }
// элемент значений сенсоров с индексом, равным индексу максимального значения ставим в 1
        sensorValues[indexOfMaxValue] = 1;

// Выведем на экран наш массив, который теперь содержит нули для всех
// датчиков, кроме максимально, который содержит 1
        for (int index = 0; index < SENSORS_COUNT; index++) {
            Serial.print(String(sensorValues[index]) + "   ");
        }
// Переведем на новую строку
        Serial.println();

    }

}
//----------------------------------------------------------------

void handleButton() {
    int currentButtonState = digitalRead(BUTTON_PIN);

    if ((buttonState == LOW) && (currentButtonState == HIGH)) {
        buttonState = HIGH;
        buttonPressed = true;
        delay(50);
    } else if ((buttonState == HIGH) && (currentButtonState == LOW)) {
        buttonState = LOW;
        delay(50);
    }
}


/*
//Первое значение в массиве принимаем за максимальное
int maxValue = sensorValues[0];
//так же запоминаем индекс найденного максимального значения
int indexOfMaxValue = 0;


}
*/
void learnDark() {
    for (int index = 0; index < SENSORS_COUNT; index++) {
        sensorDarkValues[index]
                = analogRead(sensorPins[index]);
    }
    Serial.println("");
    for (int index = 0; index < SENSORS_COUNT; index++) {
        Serial.println(String(index) + " = " + String(sensorDarkValues[index]));
    }
}

//----------------------------------------------------------------
void learnBright() {
    for (int index = 0; index < SENSORS_COUNT; index++) {
        sensorLightValues[index]
                = analogRead(sensorPins[index]);
    }
    Serial.println("");
    for (int index = 0; index < SENSORS_COUNT; index++) {
        Serial.println(String(index) + " = " + String(sensorLightValues[index]));
    }
}

void loop() {
    handleButton();
    if (buttonPressed) {
        buttonPressed = false;
        if (learnMode == 1) {
            learnDark();
        } else if (learnMode == 2) {
            learnBright();
        }

        learnMode++;
        if (learnMode > 2) {
            learnMode = 0;
        }
        setLearnLedStates();
//    readSensors();
    }
    if (learnMode == 0) {
        analyzeSensors();
    }
}
